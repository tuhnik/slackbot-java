package slackbot.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

    public static String getProperty(String path, String key) throws IOException {
        var mainProperties = new Properties();
        var file = new FileInputStream(path);
        mainProperties.load(file);
        file.close();

        return mainProperties.getProperty(key);
    }
}
