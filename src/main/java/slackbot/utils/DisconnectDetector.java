package slackbot.utils;

import com.slack.api.methods.SlackApiException;
import com.slack.api.model.event.MessageEvent;
import com.slack.api.rtm.RTMClient;
import com.slack.api.rtm.RTMEventHandler;
import com.slack.api.rtm.RTMEventsDispatcher;
import lombok.extern.slf4j.Slf4j;
import slackbot.extra.PingMessage;
import slackbot.extra.PongEvent;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class DisconnectDetector {
    static int timer = 15;

    public static void init(RTMEventsDispatcher dispatcher, RTMClient rtm) {
        handlePong(dispatcher);
        handleMessage(dispatcher);
        ticker(rtm);
    }

    static void handlePong(RTMEventsDispatcher dispatcher) {
        dispatcher.register(new RTMEventHandler<PongEvent>() {
            @Override
            public void handle(PongEvent event) {
                timer = 15;
            }
        });
    }

    static void handleMessage(RTMEventsDispatcher dispatcher) {
        dispatcher.register(new RTMEventHandler<MessageEvent>() {
            @Override
            public void handle(MessageEvent event) {
                timer = 15;
            }
        });
    }

    static void ticker(RTMClient rtm) {
        var executorService = Executors.newSingleThreadScheduledExecutor();

        executorService.scheduleAtFixedRate(() -> {
            timer = timer - 5;
            if (timer < 0) timer = 0;
            if (timer <= 5) {
                CompletableFuture.runAsync(() -> rtm.sendMessage(PingMessage.builder().id(1234L).build().toJSONString()));
            }
            if (timer == 0) {
                try {
                    rtm.disconnect();
                    rtm.reconnect();
                    log.debug("RTM reconnection successful.");
                } catch (IOException | SlackApiException | URISyntaxException | DeploymentException e) {
                    log.debug("RTM reconnection failed.");
                } finally {
                    timer = 15;
                }
            }
        }, 5, 5, TimeUnit.SECONDS);
    }
}
