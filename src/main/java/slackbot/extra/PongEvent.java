package slackbot.extra;

import com.slack.api.model.event.Event;

public class PongEvent implements Event {
    public static final String TYPE_NAME = "pong";
    private final String type = TYPE_NAME;

    @Override
    public String getType() {
        return type;
    }
}
