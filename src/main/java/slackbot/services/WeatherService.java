package slackbot.services;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class WeatherService {

    public String getWeather(String search) throws IOException {
        var encoded = URLEncoder.encode(search, StandardCharsets.UTF_8);
        var url = new URL("https://services.postimees.ee/places/v1/autocomplete/" + encoded);
        var tokener = new JSONTokener(url.openStream());
        var results = new JSONArray(tokener);
        if (results.isEmpty()) {
            throw new IOException("No results.");
        } else {
            var firstMatch = results.getJSONObject(0);
            return getForecast(firstMatch);
        }
    }

    private String getForecast(JSONObject firstMatch) throws IOException {
        var id = firstMatch.getString("id");
        var description = firstMatch.getString("description");
        var url = new URL("https://services.postimees.ee/weather/v4/place/" + id + "/forecast?type=currently&language=et");
        var tokener = new JSONTokener(url.openStream());
        var obj = new JSONObject(tokener);
        var forecast = obj.getJSONObject("forecast").getJSONArray("currently").getJSONObject(0);
        var summary = forecast.getString("summary");
        var temperature = forecast.getFloat("temperature");
        var apparentTemperature = forecast.getFloat("apparentTemperature");
        var windSpeed = forecast.getFloat("windSpeed");

        return description + ": " + summary + ", " + temperature + "°C (tajutav: " + apparentTemperature + "°C), tuule kiirus: " +
                windSpeed + " m/s";
    }
}
