package slackbot;

import com.slack.api.Slack;
import com.slack.api.rtm.RTMEventsDispatcherFactory;
import lombok.extern.slf4j.Slf4j;
import slackbot.handlers.GoodbyeHandler;
import slackbot.handlers.PingHandler;
import slackbot.handlers.WeatherHandler;
import slackbot.utils.DisconnectDetector;
import slackbot.utils.PropertiesReader;

@Slf4j
public class Bot {
    public static void main(String[] args) throws Exception {
        var key = PropertiesReader.getProperty("./slackbot.properties", "rtm.api.key");
        log.info("Bot started");

        var slack = Slack.getInstance();
        var rtm = slack.rtmConnect(key);
        var client = slack.methodsAsync(key);

        var dispatcher = RTMEventsDispatcherFactory.getInstance();

        dispatcher.register(new PingHandler(client));
        dispatcher.register(new WeatherHandler(client));
        dispatcher.register(new GoodbyeHandler(rtm));

        rtm.addMessageHandler(dispatcher.toMessageHandler());

        rtm.connect();

        DisconnectDetector.init(dispatcher, rtm);
    }
}