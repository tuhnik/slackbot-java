package slackbot.handlers;

import com.slack.api.model.event.GoodbyeEvent;
import com.slack.api.rtm.RTMClient;
import com.slack.api.rtm.RTMEventHandler;

import java.io.IOException;

public class GoodbyeHandler extends RTMEventHandler<GoodbyeEvent> {
    public RTMClient rtm;

    public GoodbyeHandler(RTMClient rtm) {
        this.rtm = rtm;
    }

    @Override
    public void handle(GoodbyeEvent event) {
        try {
            rtm.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
