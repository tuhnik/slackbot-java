package slackbot.handlers;

import com.slack.api.methods.AsyncMethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.model.event.MessageEvent;
import com.slack.api.rtm.RTMEventHandler;

import java.util.Optional;

public class PingHandler extends RTMEventHandler<MessageEvent> {

    public AsyncMethodsClient client;

    public PingHandler(AsyncMethodsClient client) {
        this.client = client;
    }

    @Override
    public void handle(MessageEvent event) {

        var text = Optional.ofNullable(event.getText()).orElse("");
        var channel = Optional.ofNullable(event.getChannel()).orElse("");

        if (text.equals("!ping")) {
            var message = ChatPostMessageRequest.builder()
                    .channel(channel)
                    .text("pong! :tada:")
                    .iconEmoji(":java:")
                    .username("Tuhnik (Java)")
                    .build();
            client.chatPostMessage(message);
        }
    }
}
