package slackbot.handlers;

import com.slack.api.methods.AsyncMethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.model.event.MessageEvent;
import com.slack.api.rtm.RTMEventHandler;
import lombok.extern.slf4j.Slf4j;
import slackbot.services.WeatherService;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class WeatherHandler extends RTMEventHandler<MessageEvent> {
    public AsyncMethodsClient client;

    public WeatherHandler(AsyncMethodsClient client) {
        this.client = client;
    }

    @Override
    public void handle(MessageEvent event) {

        var text = Optional.ofNullable(event.getText()).orElse("");
        var channel = Optional.ofNullable(event.getChannel()).orElse("");

        if (text.contains("!weather")) {
            var weatherService = new WeatherService();
            String[] splitted = text.split(" ");
            String search;

            search = splitted.length > 1 ? splitted[1] : "tallinn";

            CompletableFuture.supplyAsync(() -> {
                String result = "";
                try {
                    result = weatherService.getWeather(search);
                } catch (IOException e) {
                    var isNetworkError = !e.getMessage().equals("No results.");
                    if (isNetworkError) {
                        log.error(e.getMessage());
                        sendMessage(channel, "Päring ebaõnnestus");
                    }
                    else {
                        sendMessage(channel, "Linna/asukohta ei leitud.");
                    }
                }
                return result;
            }).thenAccept(result -> sendMessage(channel, result));

        }
    }

    public void sendMessage(String channel, String text) {
        var message = ChatPostMessageRequest.builder()
                .channel(channel)
                .text(text)
                .iconEmoji(":java:")
                .username("Tuhnik (Java)")
                .build();
        client.chatPostMessage(message);
    }
}
