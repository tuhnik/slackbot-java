package slackbot.handlers;

import com.slack.api.methods.SlackApiException;
import com.slack.api.rtm.RTMClient;
import com.slack.api.rtm.RTMCloseHandler;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.CloseReason;
import javax.websocket.DeploymentException;
import java.io.IOException;
import java.net.URISyntaxException;

@Slf4j
public class CloseHandler implements RTMCloseHandler {
    public RTMClient rtm;

    public CloseHandler(RTMClient rtm) {
        this.rtm = rtm;
    }

    @Override
    public void handle(CloseReason reason) {
        log.debug("Connection closed: " + reason.getReasonPhrase());
        while (true) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException interruptedException) {
                log.debug(interruptedException.getMessage());
            }
            try {
                rtm.reconnect();
                log.debug("Reconnected succesfully");
                break;
            } catch (IOException | SlackApiException | URISyntaxException | DeploymentException e) {
                log.debug("Couldn't reconnect... retrying");
            }
        }
    }
}
